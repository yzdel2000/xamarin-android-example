﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppRobot.Services
{
    public interface IApiService
    {
        Task<ApiResponse> GetAsyanc(string strInfo, string strLoc = "湖北武汉");
    }
}