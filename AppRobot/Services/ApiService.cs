﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppRobot.Services
{
    class ApiService
    {
        const string URL = "http://www.tuling123.com/openapi/api";
        const string KEY = "9628876498aa40ad90661e0f36e4d19c";

        static HttpClient _client = new HttpClient();

        public async Task<ApiResponse> GetAsyanc(string strInfo, string strLoc = "湖北省荆州市")
        {
            try
            {
                var postData = new Dictionary<string, string>
                {
                    ["key"] = KEY,
                    ["info"] = strInfo,
                    ["loc"] = strLoc
                };

                var response = await _client.PostAsync(URL, new FormUrlEncodedContent(postData));
                var result = await response.Content.ReadAsStringAsync();
                var json = JsonConvert.DeserializeObject<ApiResponse>(result);

                return json;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    class ApiResponse
    {
        public int Code { get; set; }
        public string Text { get; set; }
    }
}