﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppRobot.Services;
using Unity;

namespace AppRobot
{
    [Application]
    public class App : Application
    {
        public static IUnityContainer container = new UnityContainer();

        public App(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            container.RegisterType<IApiService, ApiService>();
        }
    }
}