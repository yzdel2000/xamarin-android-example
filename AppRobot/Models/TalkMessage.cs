﻿namespace AppRobot.Models
{
    enum TalkType
    {
        Send = 1,
        Receive
    }

    class TalkMessage
    {
        public TalkType Flag { get; set; }
        public string Content { get; set; }
    }
}