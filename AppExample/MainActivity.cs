﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.App;
using Google.Android.Material.BottomNavigation;
using ZXing.Mobile;

namespace AppExample
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            BottomNavigationView navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);

            // 处理安卓高版本拍照限制
            if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
            {
                var builder = new StrictMode.VmPolicy.Builder();
                StrictMode.SetVmPolicy(builder.Build());
                builder.DetectFileUriExposure();
            }

            // MobileBarcodeScanner.Initialize(Application);
            Xamarin.Essentials.Platform.Init(Application);

            SupportFragmentManager.BeginTransaction()
                .Add(Resource.Id.fragment, new CameraFragment())
                .Commit();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.navigation_home:
                    SupportFragmentManager.BeginTransaction()
                        .Replace(Resource.Id.fragment, new CameraFragment())
                        .Commit();
                    return true;
                case Resource.Id.navigation_dashboard:
                    SupportFragmentManager.BeginTransaction()
                        .Replace(Resource.Id.fragment, new GirlFragment())
                        .Commit();
                    return true;
                case Resource.Id.navigation_notifications:
                    SupportFragmentManager.BeginTransaction()
                        .Replace(Resource.Id.fragment, new QrCodeFragment())
                        .Commit();
                    return true;
            }
            return false;
        }
    }
}

