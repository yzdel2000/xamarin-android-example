﻿using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Google.Android.Material.FloatingActionButton;

namespace AppExample
{
    public class QrCodeFragment : Fragment
    {
        TextView _txSacn;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your fragment here

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_qrcode, container, false);
            _txSacn = view.FindViewById<TextView>(Resource.Id.txScan);

            var fab = view.FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += async (s, e) =>
            {
                var scanner = new ZXing.Mobile.MobileBarcodeScanner();
                var result = await scanner.Scan();

                _txSacn.Text = result != null ? result.Text : string.Empty;
            };

            return view;
        }
    }
}