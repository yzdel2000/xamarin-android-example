﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace AppExample.Models
{
    class Gank
    {
        public bool Error { get; set; }
        public Girl[] Results { get; set; }
    }

    class Girl
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Desc { get; set; }
        public DateTime PublishedAt { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public bool Used { get; set; }
        public string Who { get; set; }
    }
}