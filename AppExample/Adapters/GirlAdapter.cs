﻿using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using AppExample.Models;
using FFImageLoading;
using FFImageLoading.Cross;

namespace AppExample.Adapters
{
    class GirlAdapter : RecyclerView.Adapter
    {
        private readonly IList<Girl> _items;

        public GirlAdapter(IList<Girl> items)
        {
            _items = items;
        }

        public override int ItemCount => _items.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            (holder as ImagesViewHolder).Update(_items[position]);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            return new ImagesViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cell_girl, parent, false));
        }
    }

    class ImagesViewHolder : RecyclerView.ViewHolder
    {
        readonly MvxCachedImageView _imgGirl;
        readonly TextView _txDate;

        public ImagesViewHolder(View view) : base(view)
        {
            _imgGirl = view.FindViewById<MvxCachedImageView>(Resource.Id.imgGirl);
            _txDate = view.FindViewById<TextView>(Resource.Id.txDate);
        }

        public void Update(Girl girl)
        {
            _txDate.Text = girl.PublishedAt.ToString("yyyy年MM月dd");
            ImageService.Instance.LoadUrl(girl.Url).Into(_imgGirl);
        }
    }
}